## 最佳实践-使用存储库管理器

存储库管理器是专用于管理二进制组件存储库的服务器应用程序。

对于Maven的大量使用，使用存储库管理器被认为是必不可少的最佳实践。

### 目的

存储库管理器满足以下基本目的：

- 充当公共Maven存储库的专用代理服务器（请参阅《[Maven镜像设置指南》](https://maven.apache.org/guides/mini/guide-mirror-settings.html)）
- 提供存储库作为您的Maven项目输出的部署目标

### 优点和特点

使用存储库管理器具有以下好处和功能：

- 大大减少了从远程存储库下载的次数，节省了时间和带宽，从而提高了构建性能
- 由于减少了对外部存储库的依赖，提高了构建稳定性
- 与远程SNAPSHOT存储库交互的性能提高
- 控制消耗和提供的工件的潜力
- 创建一个中央存储并访问工件及其相关的元数据，以将构建输出暴露给其他项目和开发人员等消费者，以及质量保证或运营团队甚至客户
- 提供了一个有效的平台来交换组织内外的二进制工件，而无需从源代码构建工件

### 可用的存储库管理器

下列开源和商业存储库管理器列表（按字母顺序排列）已知支持Maven使用的存储库格式。有关一般的存储库管理以及这些产品提供的功能的更多信息，请参考相应的链接网站。

- [Apache Archiva](https://archiva.apache.org/)（开源）
- [CloudRepo](https://www.cloudrepo.io/)（商业）
- [Cloudsmith套餐](https://www.cloudsmith.io/)（商业）
- [区域](https://www.dist.cloud/)（商业）
- [JFrog Artifactory开源](https://www.jfrog.com/open-source)（开源）
- [JFrog Artifactory Pro](https://www.jfrog.com/artifactory/)（商业版）
- [MyGet](https://www.myget.org/)（商业）
- [Sonatype Nexus OSS](https://www.sonatype.org/nexus/go/)（开源）
- [Sonatype Nexus Pro](https://links.sonatype.com/products/nexus/pro/home)（商业）
- [packagecloud.io](https://packagecloud.io/)（商业）
- [Reposilite](https://reposilite.com/)（开源）