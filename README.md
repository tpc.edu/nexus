[TOC]

# 使用Nexus搭建Maven私服

## 1.私服简介

私服是架设在局域网的一种特殊的远程仓库，目的是代理远程仓库及部署第三方构件。有了私服之后，当 Maven 需要下载构件时，直接请求私服，私服上存在则下载到本地仓库；否则，私服请求外部的远程仓库，将构件下载到私服，再提供给本地仓库下载。

![无私服](https://images0.cnblogs.com/i/293735/201406/162358583957595.png)![有私服](https://images0.cnblogs.com/i/293735/201406/162359096298261.png)

我们可以使用专门的[Maven 仓库管理软件](https://maven.apache.org/repository-management.html)来搭建私服，比如：[Apache Archiva](http://archiva.apache.org/index.cgi)，[Artifactory](https://www.jfrog.com/open-source)，[Sonatype Nexus](https://blog.sonatype.com/)。这里我们使用 Sonatype Nexus。

### 1.1.Nexus简介

Nexus 是Maven仓库管理器，如果你使用Maven，你可以从Maven中央仓库 下载所需要的构件（artifact），但这通常不是一个好的做法，你应该在本地架设一个Maven仓库服务器，在代理远程仓库的同时维护本地仓库，以节省带宽和时间，Nexus就可以满足这样的需要。此外，他还提供了强大的仓库管理功能，构件搜索功能，它基于REST，友好的UI是一个extjs的REST客户端，它占用较少的内存，基于简单文件系统而非数据库。这些优点使其日趋成为最流行的Maven仓库管理器。

## 2.安装和运行

请参考官网[下载](https://help.sonatype.com/repomanager3/download)及[安装运行](https://help.sonatype.com/repomanager3/installation)教程

### 2.1.下载

可以使用[Nexus Repository Manager 3](https://download.sonatype.com/nexus/3/latest-unix.tar.gz), 或者[Nexus Repository Manager OSS](https://www.sonatype.com/products/repository-oss-download)下载最新的版本.

### 2.2.安装

使用`tar xvzf`解压下载的 GZip'd TAR 文件, 并将解压的文件放置于`/opt`目录下.
```shell
$ cp nexus-3.30.1-01-unix.tar.gz /opt
$ cd /opt
$ tar xvzf nexus-3.30.1-01-unix.tar.gz
```
> - [Nexus Repository Manager 2 安装与运行](https://help.sonatype.com/repomanager2/installing-and-running)
> - [Nexus Repository Manager 3 安装与运行](https://help.sonatype.com/repomanager3/installation)

### 2.3.运行

启动存储库管理器并在后台运行:
```shell
$ cd /opt/nexus-3.30.1-01/bin
$ ./nexus start
```
停止存储库管理器在后台运行:
```shell
$ ./nexus stop
```
查看日志:
```shell
$ tail -f logs/wrapper.log
$ tail -f /opt/sonatype-work/nexus3/log/nexus.log
```
> The `nexus` script can be used to manage the repository manager as a background application on OSX and Unix with the `start`, `stop`, `restart`, `force-reload` and `status` commands.
> [Run as a Service](https://help.sonatype.com/repomanager3/installation/run-as-a-service)
> [Configuring the Runtime Environment](https://help.sonatype.com/repomanager3/installation/configuring-the-runtime-environment)
> [Post Install Checklist](https://help.sonatype.com/repomanager3/installation/post-install-checklist)

## 3.用户界面

打开浏览器，访问：http://192.168.1.3:8081/nexus
![nexus home](https://images0.cnblogs.com/i/293735/201406/170047243957928.png)

点击右上角 Log In，使用用户名：admin ，密码：admin123 登录，可使用更多功能：

![nexus admin home](https://images0.cnblogs.com/i/293735/201406/170053363173719.png)

> The default administrator username and password combination is `admin` and `admin123` .

## 4.Nexus预置的仓库

点击左侧 Repositories 链接，查看 Nexus 内置的仓库：

![nexus repositories](https://images0.cnblogs.com/i/293735/201406/170120024571827.png)

Nexus 的仓库分为这么几类：
* hosted 宿主仓库：主要用于部署无法从公共仓库获取的构件（如 oracle 的 JDBC 驱动）以及自己或第三方的项目构件；
* proxy 代理仓库：代理公共的远程仓库；
* virtual 虚拟仓库：用于适配 Maven 1；
* group 仓库组：Nexus 通过仓库组的概念统一管理多个仓库，这样我们在项目中直接请求仓库组即可请求到仓库组管理的多个仓库。

![nexus repository type](https://images0.cnblogs.com/i/293735/201406/170144310676162.png)

## 5.配置Maven使用私服

私服搭建成功，我们就可以配置 Maven 使用私服，以后下载构件、部署构件，都通过私服来管理。

在 settings.xml 文件中，为所有仓库配置一个镜像仓库，镜像仓库的地址即私服的地址（这儿我们使用私服公共仓库组 Public Repositories 的地址）：

![nexus group](https://images0.cnblogs.com/i/293735/201406/170233181767611.png)

```xml
<mirrors>
    <mirror>
        <id>central</id>
        <mirrorOf>*</mirrorOf> <!-- * 表示让所有仓库使用该镜像--> 
        <name>central-mirror</name> 
        <url>http://localhost:8081/nexus/content/groups/public/</url>
    </mirror> 
</mirrors>
```

> Maven and Other Build Tools [Apache Maven](https://help.sonatype.com/repomanager2/maven-and-other-build-tools/apache-maven)
> [Nexus Repository Manager - Proxying Maven and npm Quick Start](https://guides.sonatype.com/repo3/quick-start-guides/proxying-maven-and-npm/)
> [Maven settings](https://blog.csdn.net/hkk666123/article/details/116905859)

**本文转载自*博客园平台**LUOTAO**的[Maven入门指南⑤：使用Nexus搭建Maven私服](https://www.cnblogs.com/luotaoyeah/p/3791966.html)一文。
